﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SplashScreen : MonoBehaviour {

    public Image Logo;
    public string Loadscene;

    private IEnumerator Start()
    {
        Logo.canvasRenderer.SetAlpha(0.0f);

        Fadein();
        yield return new WaitForSeconds(2.5f);
        Fadeout();
        yield return new WaitForSeconds(2.5f);
        SceneManager.LoadScene(Loadscene);
    }
    
    void Fadein ()
    {
        Logo.CrossFadeAlpha(1.0f, 1.5f, false);
	}

    void Fadeout()
    {
        Logo.CrossFadeAlpha(0.0f, 2.5f, false);
    }
}
